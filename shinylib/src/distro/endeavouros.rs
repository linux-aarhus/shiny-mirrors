use chrono::{DateTime, Local};
use serde::{Deserialize, Deserializer, Serialize};
use smallvec::SmallVec;
use std::{
    ops::{Deref, DerefMut},
    str::FromStr,
    time::Duration,
};
use url::Url;

use crate::{
    geo::{Continent, Country},
    mirror::Status,
    USER_AGENT,
};

use super::{Protocol, Result};

pub const DISTRO_NAME: &str = "EndeavourOS";

pub const REPO_ARCH: [&str; 2] = ["$repo", "$arch"];
pub const MIRRORLIST_URL: &str =
    "https://raw.githubusercontent.com/endeavouros-team/PKGBUILDS/master/endeavouros-mirrorlist/endeavouros-mirrorlist";
pub const MIRROR_LIST_FILE: &str = "/etc/pacman.d/endeavouros-mirrorlist";

// 1.12.2021 Around 234KB
pub const SMALL_TEST_FILE: &str = "endeavouros.db.tar.xz";
pub const SMALL_TEST_FILE_DIR: [&str; 1] = ["endeavouros"];

// 1.12.2021 Around 1.9MB
// Uses the SMALL_TEST_FILE_DIR
pub const MEDIUM_TEST_FILE: &str = "endeavouros.files.tar.xz";
pub const MEDIUM_TEST_FILE_DIR: [&str; 1] = SMALL_TEST_FILE_DIR;

// 1.12.2021 Around 5.3MB
// Uses the SMALL_TEST_FILE_DIR
pub const BIG_TEST_FILE: &str = "galaxy.files.tar.gz";
pub const BIG_TEST_FILE_DIR: [&str; 1] = SMALL_TEST_FILE_DIR;

/// List of [mirrors][Mirror]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MirrorList(Vec<Mirror>);

impl MirrorList {
    /// Download the status of mirror list
    pub fn pure_download_mirrors() -> Result<Self> {
        let v = reqwest::blocking::Client::builder()
            .user_agent(USER_AGENT)
            .build()?
            .get(MIRRORLIST_URL)
            .send()?
            .text()?;

        let mut mirrorlist = Self(Vec::new());

        let mut current_location = None;

        for line in v.lines() {
            match line {
                line if line.starts_with("Server = ") => {
                    if let Some(country) = current_location {
                        mirrorlist.push(Mirror {
                            url: line
                                .trim_start_matches("Server = ")
                                .trim()
                                .trim_end_matches(REPO_ARCH.join("/").as_str())
                                .parse()?,
                            country,
                            time: None,
                        });
                    }
                }
                line if line.starts_with("## ") => current_location = line.trim_start_matches("## ").trim().parse().ok(),
                _ => {}
            }
        }
        Ok(mirrorlist)
    }
}

impl Deref for MirrorList {
    type Target = Vec<Mirror>;

    fn deref(&self) -> &Vec<Mirror> {
        &self.0
    }
}

impl DerefMut for MirrorList {
    fn deref_mut(&mut self) -> &mut Vec<Mirror> {
        &mut self.0
    }
}

/// Mirror entry in [Mirror List][MirrorList]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Mirror {
    pub url: Url,
    pub country: Country,
    /// None == Not measured or failed to connect or such
    #[serde(skip)]
    pub time: Option<Duration>,
}
