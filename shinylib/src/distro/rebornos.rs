use chrono::{DateTime, Local};
use serde::{Deserialize, Deserializer, Serialize};
use smallvec::SmallVec;
use std::{
    ops::{Deref, DerefMut},
    str::FromStr,
    time::Duration,
};
use url::Url;

use crate::{
    geo::{Continent, Country},
    mirror::Status,
    USER_AGENT,
};

use super::{Protocol, Result};

pub const DISTRO_NAME: &str = "RebornOS";

pub const REPO_ARCH: [&str; 1] = ["RebornOS"];
pub const MIRRORLIST_URL: &str =
    "https://gitlab.com/rebornos-team/rebornos-special-system-files/mirrors/reborn-mirrorlist/-/raw/master/reborn-mirrorlist";
pub const MIRROR_LIST_FILE: &str = "/etc/pacman.d/reborn-mirrorlist";

// 1.12.2021 Around 284KB
pub const SMALL_TEST_FILE: &str = "Reborn-OS.db.tar.xz";
pub const SMALL_TEST_FILE_DIR: [&str; 0] = [];

// 1.12.2021 Around 7.1MB
// Uses the SMALL_TEST_FILE_DIR
pub const MEDIUM_TEST_FILE: &str = "Reborn-OS.files.tar.xz";
pub const MEDIUM_TEST_FILE_DIR: [&str; 0] = SMALL_TEST_FILE_DIR;

// 1.12.2021 Around 5.3MB
// Uses the SMALL_TEST_FILE_DIR
pub const BIG_TEST_FILE: &str = "";
pub const BIG_TEST_FILE_DIR: [&str; 0] = SMALL_TEST_FILE_DIR;

/// List of [mirrors][Mirror]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MirrorList(Vec<Mirror>);

impl MirrorList {
    /// Download the status of mirror list
    pub fn pure_download_mirrors() -> Result<Self> {
        let v = reqwest::blocking::Client::builder()
            .user_agent(USER_AGENT)
            .build()?
            .get(MIRRORLIST_URL)
            .send()?
            .text()?;

        let mut mirrorlist = Self(Vec::new());

        for line in v.lines() {
            if line.starts_with("Server = ") {
                mirrorlist.push(Mirror {
                    url: line.trim_start_matches("Server = ").trim().parse()?,
                    time: None,
                });
            }
        }

        Ok(mirrorlist)
    }
}

impl Deref for MirrorList {
    type Target = Vec<Mirror>;

    fn deref(&self) -> &Vec<Mirror> {
        &self.0
    }
}

impl DerefMut for MirrorList {
    fn deref_mut(&mut self) -> &mut Vec<Mirror> {
        &mut self.0
    }
}

/// Mirror entry in [Mirror List][MirrorList]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Mirror {
    pub url: Url,
    /// None == Not measured or failed to connect or such
    #[serde(skip)]
    pub time: Option<Duration>,
}
