use std::{
    fs::OpenOptions,
    io::Read,
    ops::{Deref, DerefMut},
    time::Duration,
};

use serde::{Deserialize, Deserializer, Serialize};
use smallvec::SmallVec;
use strum::{Display, EnumString, EnumVariantNames};
use url::Url;

use crate::{geo::Country, ShinyError, USER_AGENT};

use super::{Protocol, ShinyResult, Status};

pub const DISTRO_NAME: &str = "Manjaro";

pub const REPO_ARCH: [&str; 2] = ["$repo", "$arch"];
pub const MIRRORLIST_URL: &str = "https://repo.manjaro.org/status.json";
pub const MIRROR_LIST_FILE: &str = "/etc/pacman.d/mirrorlist";

// 6.10.2021 Around 176KB
pub const SMALL_TEST_FILE: &str = "core.db.tar.gz";
pub const SMALL_TEST_FILE_DIR: [&str; 1] = ["core"];

// 2.7.2023 Around 8.6MB
// Uses the BIG_TEST_FILE_DIR
pub const MEDIUM_TEST_FILE: &str = "extra.db.tar.gz";
pub const MEDIUM_TEST_FILE_DIR: [&str; 1] = BIG_TEST_FILE_DIR;

// 2.7.2023 Around 40MB
pub const BIG_TEST_FILE: &str = "extra.files.tar.gz";
pub const BIG_TEST_FILE_DIR: [&str; 1] = ["extra"];

/// List of [mirrors][Mirror]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MirrorList(Vec<Mirror>);

impl MirrorList {
    /// Download the status of mirror list
    pub fn pure_download_mirrors() -> ShinyResult<Self> {
        Ok(reqwest::blocking::Client::builder()
            .user_agent(USER_AGENT)
            .build()?
            .get(MIRRORLIST_URL)
            .send()?
            .json()?)
    }
}

impl Deref for MirrorList {
    type Target = Vec<Mirror>;

    fn deref(&self) -> &Vec<Mirror> {
        &self.0
    }
}

impl DerefMut for MirrorList {
    fn deref_mut(&mut self) -> &mut Vec<Mirror> {
        &mut self.0
    }
}

/// Mirror entry in [Mirror List][MirrorList]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Mirror {
    pub url: Url,
    pub country: Country,
    /// General Positions: 0 == Stable, 1 == Testing, 2 == Unstable
    pub branches: SmallVec<[Status; 3]>,
    /// None == Not measured or failed to connect or such
    #[serde(skip)]
    pub time: Option<Duration>,
    /// None == Unknown | Couldn't parse
    #[serde(deserialize_with = "parse_last_sync")]
    pub last_sync: Option<Duration>,
    pub protocols: SmallVec<[Protocol; 4]>,
}

impl Mirror {
    pub fn get_status(&self, branch: Branch) -> Status {
        #[cfg(any(target_arch = "x86_64", target_arch = "x86"))]
        let index = match branch {
            Branch::Stable => 0,
            Branch::Testing => 1,
            Branch::Unstable => 2,
        };

        #[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
        let index = match branch {
            Branch::ArmStable => 0,
            Branch::ArmTesting => 1,
            Branch::ArmUnstable => 2,
        };

        self.branches.get(index).copied().unwrap_or_default()
    }
}

fn parse_last_sync<'de, D>(deserializer: D) -> Result<Option<Duration>, D::Error>
where
    D: Deserializer<'de>,
{
    // BECAUSE THE VALUE CAN BE TWO DATA TYPES FOR SOME REASON, WHY EVERYONE DOES THIS
    // Basically, normally it's in String, but when it's unknown it's -1 int, yeah
    // clippy is shouting at me
    let item: String = match Deserialize::deserialize(deserializer) {
        Ok(v) => v,
        Err(_) => return Ok(None),
    };

    Ok(item
        .split_once(':')
        .and_then(|(hours, minutes)| match (hours.parse::<u64>(), minutes.parse::<u64>()) {
            (Ok(hours), Ok(minutes)) => Some(Duration::from_secs((hours * 3_600) + (minutes * 60))),
            _ => None,
        }))
}

/// Branches Manjaro has
#[cfg(any(target_arch = "x86_64", target_arch = "x86"))]
#[derive(Copy, Clone, Debug, Display, EnumVariantNames, EnumString, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "kebab-case")]
#[strum(serialize_all = "title_case")]
#[strum(ascii_case_insensitive)]
#[non_exhaustive]
pub enum Branch {
    Stable,
    Testing,
    Unstable,
}

/// Branches Manjaro has
#[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
#[derive(Copy, Clone, Debug, Display, EnumVariantNames, EnumString, Serialize, Deserialize, PartialEq, Eq, ArgEnum)]
#[serde(rename_all = "kebab-case")]
#[strum(serialize_all = "title_case")]
#[strum(ascii_case_insensitive)]
#[non_exhaustive]
pub enum Branch {
    #[strum(serialize = "Stable", to_string = "Stable")]
    ArmStable,
    #[strum(serialize = "Testing", to_string = "Testing")]
    ArmTesting,
    #[strum(serialize = "Unstable", to_string = "Unstable")]
    ArmUnstable,
}

impl Branch {
    pub fn get_branch_from_local_mirror_list() -> ShinyResult<Self> {
        let mut file = OpenOptions::new()
            .read(true)
            .write(false)
            .open(MIRROR_LIST_FILE)
            .map_err(|_| ShinyError::NoLocalMirrorListFile)?;

        let mut string = String::new();
        file.read_to_string(&mut string)?;

        Self::get_branch_from_local_mirror_list_string(&string)
    }

    pub fn get_branch_from_local_mirror_list_string(content: &str) -> ShinyResult<Self> {
        if content.is_empty() {
            return Err(ShinyError::LocalMirrorListFileEmpty)?;
        }

        // FIXME: I don't like this...
        let branch = content.find(format!("/{}", REPO_ARCH.join("/")).as_str()).and_then(|i| {
            content.get(..i).and_then(|string| {
                string
                    .rsplit_once('/')
                    .and_then(|(_, branch_str)| if branch_str.is_empty() { None } else { Some(branch_str) })
            })
        });

        let branch = serde_plain::from_str::<Self>(branch.ok_or(ShinyError::NoBranchInLocalMirrorList)?)?;

        Ok(branch)
    }
}
